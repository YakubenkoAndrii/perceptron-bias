/**
 * Created by Andrii on 2/28/17.
 */

public class Main {
    public static void main(String[] args) {
        getKnowledge();
        appliedToMySample();
    }

    private static double[] getKnowledge() {                    //метод, що вираховує вагові конфіцієнти
                                                                // за допомогою даних з навчальної вибірки
        double W1 = (0 + Math.random() * 10) / 1000;
        double W2 = (0 + Math.random() * 10) / 1000;
        double W3 = (0 + Math.random() * 10) / 1000;

        final double[] parseArrayXX = {0.376, 0.836, 0.267, 0.526, 0.437, 0.542, 0.855, 0.604, 0.989, 0.929, 0.809,
                0.236, 0.784, 0.745, 0.881, 0.174, 0.571, 0.741, 0.821, 0.166, 0.1, 0.666, 0.377, 0.962, 0.121, 0.272,
                0.301, 0.313, 0.384, 0.306, 0.341, 0.969, 0.64, 0.521, 0.249, 0.68, 0.638, 0.665, 0.774, 0.196, 0.128,
                0.489, 0.667, 0.266, 0.86, 0.743, 0.676, 0.64, 0.584, 0.176, 0.553, 0.07, 0.051, 0.539, 0.047, 0.304,
                0.01, 0.582, 0.908, 0.093, 0.211, 0.055, 0.886, 0.186, 0.404, 0.707, 0.732, 0.334, 0.339, 0.288, 0.194,
                0.287, 0.767, 0.55, 0.024, 0.795, 0.436, 0.241, 0.971, 0.607};
        final double[] parseArrayYY = {0.823, 0.529, 0.451, 0.835, 0.521, 0.694, 0.629, 0.449, 0.868, 0.944, 0.831,
                0.524, 0.613, 0.031, 0.806, 0.81, 0.506, 0.704, 0.305, 0.057, 0.298, 0.712, 0.077, 0.014, 0.738, 0.807,
                0.873, 0.433, 0.576, 0.737, 0.442, 0.063, 0.564, 0.253, 0.535, 0.653, 0.449, 0.106, 0.901, 0.955, 0.983,
                0.044, 0.29, 0.717, 0.915, 0.818, 0.453, 0.691, 0.996, 0.178, 0.139, 0.625, 0.975, 0.874, 0.099, 0.27,
                0.603, 0.656, 0.082, 0.376, 0.629, 0.274, 0.839, 0.694, 0.804, 0.478, 0.119, 0.972, 0.877, 0.018, 0.672,
                0.961, 0.598, 0.644, 0.249, 0.411, 0.718, 0.105, 0.599, 0.071};
        final int[] classArray = {0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1,
                0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 0,
                1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1};

        double testVariable;
        int error;
        int count = 0;
        double randValue = 0.001;
        while (count < 1000) {
            error = 0;
            for (int i = 0; i < 80; i++) {

                double x1 = parseArrayXX[i];
                double x2 = parseArrayYY[i];
                double yClass = classArray[i];
                if ((((W1 * x1) + (W2 * x2)) - W3 * 1) < 0) {
                    testVariable = 0.0;
                } else {
                    testVariable = 1.0;
                }

                if (testVariable != yClass) {
                    error++;
                    W1 = W1 + randValue * (classArray[i] - testVariable) * x1;
                    W2 = W2 + randValue * (classArray[i] - testVariable) * x2;
                    W3 = W3 - randValue * (classArray[i] - testVariable) * 1;
                }
            }
            count++;
        }
        return new double[]{W1, W2, W3};
    }

    private static void appliedToMySample() {                   //метод, що перевіряє заздалегідь знайдені вагові
                                                                // коефіцієнти на реальній вибірці даних
        double result[] = getKnowledge();
        double[] parseArrayX = {0.208, 0.247, 0.586, 0.207, 0.057, 0.805, 0.588, 0.761, 0.649, 0.293, 0.207, 0.12,
                0.732, 0.461, 0.193, 0.034, 0.058, 0.058, 0.296, 0.253};
        double[] parseArrayY = {0.81, 0.744, 0.66, 0.429, 0.516, 0.729, 0.971, 0.988, 0.12, 0.05, 0.556, 0.258,
                0.373, 0.457, 0.164, 0.99, 0.024, 0.7, 0.546, 0.932};
        int[] classArray = {0, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0};

        double W1 = result[0];
        double W2 = result[1];
        double W3 = result[2];

        System.out.println("вагові коефіцієнти після навчання на навчальній вибірці: " +
                "\nW1 = " + W1 +
                "\nW2 = " + W2 +
                "\nW3 = " + W3);

        int testVariable;
        int error = 0;

        for (int i = 0; i < 20; i++) {
            double x1 = parseArrayX[i];
            double x2 = parseArrayY[i];
            double yClass = classArray[i];
            if ((((W1 * x1) + (W2 * x2)) - W3 * 1) < 0) {
                testVariable = 0;
            } else {
                testVariable = 1;
            }
            if (testVariable != yClass) {
                error++;
            }
        }
        System.out.println("кількість помилок в тестовій вибірці з ваговими коефіцієнтами " +
                "які ми отримали при навчанні: " + error);
    }
}


